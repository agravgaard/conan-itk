#!/bin/bash
set -xe

conan user -p $CONAN_API_KEY -r conan-cbctrecon agravgaard
conan remote add conan-cbctrecon https://api.bintray.com/conan/agravgaard/cbctrecon
conan remote add sight https://conan.ircad.fr/artifactory/api/conan/sight
conan create . conan-cbctrecon/testing --build=missing
conan upload itk/5.0.0@conan-cbctrecon/testing --all -r=conan-cbctrecon
