import os
import re
import shutil

from conans import ConanFile, CMake, tools
from fnmatch import fnmatch


class LibITKConan(ConanFile):
    name = "itk"
    package_revision = ""
    upstream_version = "5.0.0"
    version = "{0}{1}".format(upstream_version, package_revision)

    generators = "cmake"
    settings = "os", "arch", "compiler", "build_type"
    options = {
        "shared": [True, False],
        "cuda": [True, False]
    }
    default_options = tuple([
        "shared=True",
        "cuda=False"
    ])

    exports = [
        "CMakeProjectWrapper.txt",
    ]
    url = "https://gitlab.com/agravgaard/conan-itk"
    license = "http://www.itk.org/licensing/"
    description = "Insight Segmentation and Registration Toolkit"
    source_subfolder = "source_subfolder"
    build_subfolder = "build_subfolder"
    short_paths = True

    def configure(self):
        del self.settings.compiler.libcxx
        if 'CI' not in os.environ:
            os.environ["CONAN_SYSREQUIRES_MODE"] = "verify"

    def requirements(self):
        self.requires("gdcm/2.8.9@sight/stable")
        self.requires("vtk/8.2.0@sight/stable")

        if not tools.os_info.is_linux:
            self.requires("libjpeg/9c-r1@sight/stable")
            self.requires("libpng/1.6.34-r1@sight/stable")
            self.requires("libtiff/4.0.9-r1@sight/stable")

        if tools.os_info.is_windows:
            self.requires("expat/2.2.5-r1@sight/stable")
            self.requires("zlib/1.2.11-r1@sight/stable")
            self.requires("fftw/3.3.8@bincrafters/stable")
            self.requires("eigen/3.3.7@conan/stable")

    def build_requirements(self):
        if tools.os_info.linux_distro == "linuxmint":
            pack_names = [
                "libexpat1-dev",
                "libjpeg-turbo8-dev",
                "libtiff5-dev",
                "libfftw3-dev",
                "libeigen3-dev"
            ]
            if tools.os_info.os_version.major(fill=False) == "18":
                pack_names.append("libpng12-dev")
            elif tools.os_info.os_version.major(fill=False) == "19":
                pack_names.append("libpng-dev")
            installer = tools.SystemPackageTool()
            for p in pack_names:
                installer.install(p)
        elif tools.os_info.linux_distro == "ubuntu":
            pack_names = [
                # Deps for Qt
                "ccache", "libgstreamer1.0-0", "gstreamer1.0-libav", "gstreamer1.0-qt5",
                "libxcb1-dev", "libx11-xcb-dev", "libglu1-mesa-dev", "libxrender-dev", "libxi-dev",
                "libudev-dev", "libinput-dev", "libts-dev", "libxcb-xinerama0-dev", "libxcb-xinerama0",
                "libxkbcommon-dev", "libxkbcommon-x11-0", "libxkbcommon-x11-dev", "libxkbcommon0",
                # For Qt WebKit
                "gperf", "bison", "flex", "libicu-dev", "libxslt-dev", "ruby",
                # For Qt WebEngine
                "libssl-dev", "libxcursor-dev", "libxcomposite-dev", "libxdamage-dev", "libxrandr-dev",
                "libdbus-1-dev", "libfontconfig1-dev", "libcap-dev", "libxtst-dev", "libpulse-dev",
                "libudev-dev", "libpci-dev", "libnss3-dev", "libasound2-dev", "libxss-dev", "libegl1-mesa-dev"
            ]
            installer = tools.SystemPackageTool()
            for p in pack_names:
                installer.install(p)

    def system_requirements(self):
        if tools.os_info.linux_distro == "linuxmint":
            pack_names = [
                "libexpat1",
                "libjpeg-turbo8",
                "libtiff5",
                "libfftw3-dev",
                "libeigen3-dev"
            ]
            if tools.os_info.os_version.major(fill=False) == "18":
                pack_names.append("libpng12-0")
            elif tools.os_info.os_version.major(fill=False) == "19":
                pack_names.append("libpng16-16")
            installer = tools.SystemPackageTool()
            for p in pack_names:
                installer.install(p)

    def source(self):
        tools.get("https://github.com/InsightSoftwareConsortium/ITK/releases/download/v5.0.0/InsightToolkit-{0}.tar.gz".format(self.upstream_version))
        os.rename("InsightToolkit-" + self.upstream_version, self.source_subfolder)

    def build(self):
        itk_source_dir = os.path.join(self.source_folder, self.source_subfolder)
        shutil.move("CMakeProjectWrapper.txt", "CMakeLists.txt")
        # tools.patch(itk_source_dir, "patches/linux.diff")

        cmake = CMake(self)
        cmake.definitions["BUILD_EXAMPLES"] = "OFF"
        cmake.definitions["BUILD_SHARED_LIBS"] = self.options.shared
        cmake.definitions["BUILD_TESTING"] = "OFF"
        cmake.definitions["BUILD_DOCUMENTATION"] = "OFF"
        cmake.definitions["ITK_USE_REVIEW"] = "ON"
        cmake.definitions["ITK_SKIP_PATH_LENGTH_CHECKS"] = "ON"

        cmake.definitions["ITK_USE_SYSTEM_DCMTK"] = "ON"
        cmake.definitions["ITK_USE_SYSTEM_FFTW"] = "ON"
        cmake.definitions["ITK_USE_SYSTEM_GDCM"] = "ON"
        cmake.definitions["ITK_USE_SYSTEM_PNG"] = "ON"
        cmake.definitions["ITK_USE_SYSTEM_TIFF"] = "ON"
        cmake.definitions["ITK_USE_SYSTEM_ZLIB"] = "ON"
        cmake.definitions["ITK_USE_SYSTEM_JPEG"] = "ON"
        cmake.definitions["ITK_USE_SYSTEM_EXPAT"] = "ON"
        cmake.definitions["ITK_USE_GPU"] = "ON"
        cmake.definitions["ITK_USE_FFTWD"] = "ON"
        cmake.definitions["ITK_USE_FFTWF"] = "ON"

        cmake.definitions["ITK_BUILD_DEFAULT_MODULES"] = "OFF"

        cmake.definitions["Module_AnalyzeObjectMapIO"] = "OFF"
        cmake.definitions["Module_AnisotropicDiffusionLBR"] = "OFF"
        cmake.definitions["Module_BSplineGradient"] = "OFF"
        cmake.definitions["Module_BioCell"] = "OFF"
        cmake.definitions["Module_BoneMorphometry"] = "OFF"
        cmake.definitions["Module_Cuberille"] = "OFF"
        cmake.definitions["Module_FixedPointInverseDisplacementField"] = "OFF"
        cmake.definitions["Module_GenericLabelInterpolator"] = "OFF"
        cmake.definitions["Module_HigherOrderAccurateGradient"] = "OFF"
        cmake.definitions["Module_IOFDF"] = "OFF"
        cmake.definitions["Module_IOMeshSTL"] = "OFF"
        cmake.definitions["Module_IOOpenSlide"] = "OFF"
        cmake.definitions["Module_IOScanco"] = "OFF"
        cmake.definitions["Module_IOTransformDCMTK"] = "OFF"
        cmake.definitions["Module_ITKBridgeNumPy"] = "OFF"
        cmake.definitions["Module_ITKColormap"] = "OFF"
        cmake.definitions["Module_ITKColormap"] = "OFF"
        cmake.definitions["Module_ITKDICOMParser"] = "OFF"
        cmake.definitions["Module_ITKDeconvolution"] = "OFF"
        cmake.definitions["Module_ITKDenoising"] = "OFF"
        cmake.definitions["Module_ITKFEM"] = "OFF"
        cmake.definitions["Module_ITKFEMRegistration"] = "OFF"
        cmake.definitions["Module_ITKGoogleTest"] = "OFF"
        cmake.definitions["Module_ITKIOBruker"] = "OFF"
        cmake.definitions["Module_ITKIODCMTK"] = "OFF"
        cmake.definitions["Module_ITKIOHDF5"] = "OFF"
        cmake.definitions["Module_ITKIOJPEG2000"] = "OFF"
        cmake.definitions["Module_ITKIOLSM"] = "OFF"
        cmake.definitions["Module_ITKIOMINC"] = "OFF"
        cmake.definitions["Module_ITKIOMRC"] = "OFF"
        cmake.definitions["Module_ITKIOMesh"] = "OFF"
        cmake.definitions["Module_ITKIOPhilipsREC"] = "OFF"
        cmake.definitions["Module_ITKIOTransformMINC"] = "OFF"
        cmake.definitions["Module_ITKImageFrequency"] = "OFF"
        cmake.definitions["Module_ITKIntegratedTest"] = "OFF"
        cmake.definitions["Module_ITKLIBLBFGS"] = "OFF"
        cmake.definitions["Module_ITKLevelSetsv4"] = "OFF"
        cmake.definitions["Module_ITKMINC"] = "OFF"
        cmake.definitions["Module_ITKMetricsv4"] = "OFF"
        cmake.definitions["Module_ITKOptimizersv4"] = "OFF"
        cmake.definitions["Module_ITKRegistrationMethodsv4"] = "OFF"
        cmake.definitions["Module_ITKSuperPixel"] = "OFF"
        cmake.definitions["Module_ITKTBB"] = "OFF"
        cmake.definitions["Module_ITKVideoBridgeOpenCV"] = "OFF"
        cmake.definitions["Module_ITKVideoBridgeVXL"] = "OFF"
        cmake.definitions["Module_ITKVideoCore"] = "OFF"
        cmake.definitions["Module_ITKVideoFiltering"] = "OFF"
        cmake.definitions["Module_ITKVideoIO"] = "OFF"
        cmake.definitions["Module_ITKVtkGlue"] = "OFF" # Will probably cause linker errors if on
        cmake.definitions["Module_IsotropicWavelets"] = "OFF"
        cmake.definitions["Module_LabelErodeDilate"] = "OFF"
        cmake.definitions["Module_LesionSizingToolkit"] = "OFF"
        cmake.definitions["Module_MGHIO"] = "OFF"
        cmake.definitions["Module_MeshNoise"] = "OFF"
        cmake.definitions["Module_MinimalPathExtraction"] = "OFF"
        cmake.definitions["Module_Montage"] = "OFF"
        cmake.definitions["Module_MorphologicalContourInterpolation"] = "OFF"
        cmake.definitions["Module_MultipleImageIterator"] = "OFF"
        cmake.definitions["Module_NeuralNetworks"] = "OFF"
        cmake.definitions["Module_ParabolicMorphology"] = "OFF"
        cmake.definitions["Module_PerformanceBenchmarking"] = "OFF"
        cmake.definitions["Module_PhaseSymmetry"] = "OFF"
        cmake.definitions["Module_PolarTransform"] = "OFF"
        cmake.definitions["Module_PrincipalComponentAnalysis"] = "OFF"
        cmake.definitions["Module_RLEImage"] = "OFF"
        cmake.definitions["Module_SCIFIO"] = "OFF"
        cmake.definitions["Module_SimpleITKFilters"] = "OFF"
        cmake.definitions["Module_SkullStrip"] = "OFF"
        cmake.definitions["Module_SmoothingRecursiveYvvGaussianFilter"] = "OFF"
        cmake.definitions["Module_SphinxExamples"] = "OFF"
        cmake.definitions["Module_SplitComponents"] = "OFF"
        cmake.definitions["Module_Strain"] = "OFF"
        cmake.definitions["Module_SubdivisionQuadEdgeMeshFilter"] = "OFF"
        cmake.definitions["Module_TextureFeatures"] = "OFF"
        cmake.definitions["Module_Thickness3D"] = "OFF"
        cmake.definitions["Module_TotalVariation"] = "OFF"
        cmake.definitions["Module_TwoProjectionRegistration"] = "OFF"
        cmake.definitions["Module_VariationalRegistration"] = "OFF"
        cmake.definitions["Module_WikiExamples"] = "OFF"

        # Required Modules:
        cmake.definitions["Module_RTK"]           = "ON" # Required for CbctRecon
        cmake.definitions["RTK_USE_CUDA"]         = "ON" if self.options.cuda else "OFF"
        cmake.definitions["Module_ITKCudaCommon"] = "ON" if self.options.cuda else "OFF"
        cmake.definitions["Module_ITKDeprecated"] = "ON" # Required for Plastimatch
        cmake.definitions["Module_ITKReview"]     = "ON" # Required for Plastimatch
        cmake.definitions["Module_ITKDCMTK"]      = "ON" # Required for Plastimatch & CbctRecon

        # May be required in the future:
        cmake.definitions["Module_ITKVTK"] = "ON"
        cmake.definitions["Module_ITKGPUAnisotropicSmoothing"] = "ON"
        cmake.definitions["Module_ITKGPUImageFilterBase"] = "ON"
        cmake.definitions["Module_ITKGPUPDEDeformableRegistration"] = "ON"
        cmake.definitions["Module_ITKGPURegistrationCommon"] = "ON"
        cmake.definitions["Module_ITKGPUSmoothing"] = "ON"
        cmake.definitions["Module_ITKGPUThresholding"] = "ON"

        if not tools.os_info.is_windows:
            cmake.definitions["CMAKE_POSITION_INDEPENDENT_CODE"] = "ON"
        cmake.configure(build_folder=self.build_subfolder)
        cmake.build()
        cmake.install()

    def cmake_fix_macos_sdk_path(self, file_path):
        # Read in the file
        with open(file_path, 'r') as file:
            file_data = file.read()

        if file_data:
            # Replace the target string
            file_data = re.sub(
                # Match sdk path
                r';/Applications/Xcode\.app/Contents/Developer/Platforms/MacOSX\.platform/Developer/SDKs/MacOSX\d\d\.\d\d\.sdk/usr/include',
                '',
                file_data,
                re.M
            )

            # Write the file out again
            with open(file_path, 'w') as file:
                file.write(file_data)

    def cmake_fix_path(self, file_path, package_name):
        try:
            tools.replace_in_file(
                file_path,
                self.deps_cpp_info[package_name].rootpath.replace('\\', '/'),
                "${CONAN_" + package_name.upper() + "_ROOT}",
                strict=False
            )
        except:
            self.output.info("Ignoring {0}...".format(package_name))

    def package(self):
        for path, subdirs, names in os.walk(self.package_folder):
            for name in names:
                if fnmatch(name, '*.cmake'):
                    cmake_file = os.path.join(path, name)

                    tools.replace_in_file(
                        cmake_file,
                        self.package_folder.replace('\\', '/'),
                        '${CONAN_ITK_ROOT}',
                        strict=False
                    )

                    self.cmake_fix_path(cmake_file, "gdcm")
                    self.cmake_fix_path(cmake_file, "libjpeg")
                    self.cmake_fix_path(cmake_file, "libpng")
                    self.cmake_fix_path(cmake_file, "libtiff")
                    self.cmake_fix_path(cmake_file, "zlib")
                    self.cmake_fix_path(cmake_file, "expat")

                    if tools.os_info.is_macos:
                        self.cmake_fix_macos_sdk_path(cmake_file)

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)
